package com.joselima.gpstracker;

public class Event<T> {

    public final T Content;

    public Event(T content ){

        Content = content;
    }
}
