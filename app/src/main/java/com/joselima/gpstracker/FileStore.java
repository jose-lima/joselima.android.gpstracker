package com.joselima.gpstracker;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Environment;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FileStore implements EventHandler<Permission>{

    private final Activity activity;
    private File appDirectory;
    private File currentFile;
    SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    private String gpsFileHeader = "timesStamp("+timeStampFormat.toPattern()+"), lat, long, alt(m), speed(km/h)";
    DecimalFormat threeDecimalsFormat = new DecimalFormat("####0.000");
    private boolean permissionGranted = false;

    public FileStore(Activity activity, PermissionsGuard permissions){
        this.activity = activity;

        permissions.AddHandler( this );
        if (permissions.RequestForWriting()) {
            permissionGranted = true;
            SetupDirectory();
        }
    }

    private void SetupDirectory() {
        if (!permissionGranted){
            return;
        }

        File baseDirectory = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOCUMENTS );
        appDirectory = new File(baseDirectory, "joselima.gpstracker");
        if (! appDirectory.exists()){
            appDirectory.mkdir();
        }
    }

    public void AppendGpsPoint( Location location ) throws IOException {
        if (!permissionGranted){
            return;
        }

        String gpsString = GpsToString(location);
        AppendLine(gpsString);
    }

    public String GpsToString( Location location ) {
        Date date = Calendar.getInstance().getTime();
        String currentTimeStr = timeStampFormat.format(date);
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        double alt = location.getAltitude();
        double speedKmh = location.getSpeed() * 3.6;
        String gpsString = currentTimeStr
                + ", " + lat
                + ", " + lon
                + ", " + threeDecimalsFormat.format(alt)
                + ", " + threeDecimalsFormat.format(speedKmh);
        return gpsString;
    }

    public void AppendLine( String dataPoint ) throws IOException {
        if (!permissionGranted){
            return;
        }

        if (currentFile == null){
            StartNewFile();
        }

        try {
            FileWriter fw = new FileWriter(currentFile, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(dataPoint);
            bw.newLine();
            bw.close();
        } catch (IOException exc){
            throw exc;
        }
    }

    public void StartNewFile() throws IOException {
        if (!permissionGranted){
            return;
        }
        String currentFilePath = NewFileName();
        currentFile = new File(appDirectory, currentFilePath);
        AppendLine(gpsFileHeader);
    }

    private String NewFileName() {
        Date date = Calendar.getInstance().getTime();
        String timeStamp = timeStampFormat.format(date);
        return "gpsPoints."+timeStamp+".txt";
    }

    @Override
    public void Handle(Event<Permission> event) {
        if (event.Content.RequestCode == PermissionsGuard.PERMISSIONS_EXTERNAL_STORAGE ) {
            if (event.Content.GrantResults.length > 0 && event.Content.GrantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permissionGranted = true;
                SetupDirectory();
            }
            else {
                permissionGranted = false;
            }
        }
    }
}
