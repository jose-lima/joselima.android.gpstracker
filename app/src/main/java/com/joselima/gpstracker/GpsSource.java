package com.joselima.gpstracker;
import android.app.Activity;
import android.content.pm.PackageManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;


public class GpsSource implements EventHandler<Permission> {

    private static final int GPS_UPDATE_INTERVAL = 1000; //milliseconds
    FusedLocationProviderClient locationProvider;
    private Activity activity;
    private final LocationCallback locationCallback;

    public GpsSource(Activity activity, PermissionsGuard permissionsGuard, LocationCallback locationCallback ){
        this.activity = activity;
        this.locationCallback = locationCallback;

        permissionsGuard.AddHandler( this );
        if (permissionsGuard.RequestForLocation()) {
            SetupLocationProvider();
        }
    }

    @Override
    public void Handle(Event<Permission> event) {
        if (event.Content.RequestCode == PermissionsGuard.PERMISSIONS_FINE_LOCATION) {
            if (event.Content.GrantResults.length > 0 && event.Content.GrantResults[0] == PackageManager.PERMISSION_GRANTED) {
                {
                    SetupLocationProvider();
                }
            }else {
                activity.finish();
            }
        }
    }

    private void SetupLocationProvider() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(GPS_UPDATE_INTERVAL);
        locationRequest.setFastestInterval(GPS_UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationProvider = LocationServices.getFusedLocationProviderClient( this.activity );
        locationProvider.requestLocationUpdates( locationRequest, locationCallback, null);
    }
}

