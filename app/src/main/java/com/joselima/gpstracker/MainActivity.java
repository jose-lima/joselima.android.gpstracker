package com.joselima.gpstracker;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    // ui elements
    TextView gpsTimestamp;
    TextView gpsLat;
    TextView gpsLong;
    TextView gpsAlt;
    TextView gpsSpeed;

    //
    FileStore fileStore;
    PermissionsGuard permissionsGuard;
    GpsSource gpsSource;
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init UI pointers
        gpsTimestamp = findViewById(R.id.gpsTimestamp);
        gpsLat = findViewById(R.id.gpsLat);
        gpsLong = findViewById(R.id.gpsLong);
        gpsAlt = findViewById(R.id.gpsAlt);
        gpsSpeed = findViewById(R.id.gpsSpeed);

        // init permissions
        try {
            permissionsGuard = new PermissionsGuard(this);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
            finish();
        }

        //init file store
        fileStore = new FileStore(this, permissionsGuard );

        //init gps source
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
            OnLocationResult( locationResult );
            }
        };
        gpsSource = new GpsSource( this, permissionsGuard, locationCallback );

    } // end OnCreate method

    public void OnLocationResult(LocationResult locationResult ){

        if (locationResult == null) {
            Toast.makeText(this, "locationResult object was null", Toast.LENGTH_SHORT);
            return;
        }

        for (Location location : locationResult.getLocations()) {
            if (location != null) {
                UpdateLocation(location);
            }
        }
    }

    private void UpdateLocation(Location location) {
        if (location == null) {
            return;
        }

        // write to file
        try {
            fileStore.AppendGpsPoint(location);
        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
        }

        // update UI
        Date date = Calendar.getInstance().getTime();
        String currentTimeStr = timeFormat.format(date);
        gpsTimestamp.setText( currentTimeStr );
        gpsLat.setText( String.valueOf(location.getLatitude()) );
        gpsLong.setText( String.valueOf(location.getLongitude()) );
        gpsAlt.setText( String.format("%.0f m",location.getAltitude()) );
        gpsSpeed.setText( String.format("%.2f km/h", location.getSpeed() * 3.6 ) );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permission permission = new Permission( requestCode, grantResults );
        Event event = new Event<>( permission );
        permissionsGuard.Handle( event );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
