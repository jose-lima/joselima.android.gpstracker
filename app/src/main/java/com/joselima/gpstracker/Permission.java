package com.joselima.gpstracker;

public class Permission {

    public final int RequestCode;
    public final int[] GrantResults;

    public Permission(int requestCode, int[] grantResults){

        this.RequestCode = requestCode;
        this.GrantResults = grantResults;
    }
}
