package com.joselima.gpstracker;

public interface EventHandler<T> {

    void Handle( Event<T> event);
}
