package com.joselima.gpstracker;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.List;

public class PermissionsGuard implements EventHandler<Permission> {

    public static final int PERMISSIONS_FINE_LOCATION = 1;
    public static final int PERMISSIONS_EXTERNAL_STORAGE = 2;
    private final Activity activity;
    private List<EventHandler<Permission>> permissionHandlers = new ArrayList<>();

    public PermissionsGuard(Activity activity ) throws Exception {
        this.activity = activity;
    }

    public boolean RequestForLocation() {
        return Request(Manifest.permission.ACCESS_FINE_LOCATION, PERMISSIONS_FINE_LOCATION);
    }

    public boolean RequestForWriting(){
        return Request(Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSIONS_EXTERNAL_STORAGE);
    }

    public void AddHandler(EventHandler<Permission> handler){
        permissionHandlers.add(handler);
    }

    private boolean Request( String permission, int id ){
        if ( ActivityCompat.checkSelfPermission(activity, permission)
                == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        activity.requestPermissions( new String[]{permission}, id);
        return false;
    }

    public void OnPermissionResult() {

       /* switch (requestCode){

            case PermissionsGuard.PERMISSIONS_FINE_LOCATION:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText( this, "This app requires 'ACCESS_FINE_LOCATION' permission.",
                            Toast.LENGTH_SHORT);
                    finish();
                }
                break;


            case PermissionsGuard.PERMISSIONS_EXTERNAL_STORAGE:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText( this, "This app requires 'PERMISSIONS_EXTERNAL_STORAGE' permission.",
                            Toast.LENGTH_SHORT);
                    finish();
                }
                break;
        }*/
    }

    @Override
    public void Handle(Event<Permission> event) {
        for (EventHandler<Permission> handler : permissionHandlers) {
            handler.Handle( event );
        }
    }
}
